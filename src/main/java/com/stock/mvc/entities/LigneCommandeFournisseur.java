package com.stock.mvc.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@SuppressWarnings("serial")
public class LigneCommandeFournisseur implements Serializable {

	@Id
	@GeneratedValue
	private Long idLigneCmdFourniseur;
	@ManyToOne
	@JoinColumn(name = "idArticle")
	private Article article;
	@ManyToOne
	@JoinColumn(name = "idCmdFournisseur")
	private CommandeFournisseur commandeFournisseur;

	public Long getIdLigneCmdFourniseur() {
		return idLigneCmdFourniseur;
	}

	public void setIdLigneCmdFourniseur(Long idLigneCmdFourniseur) {
		this.idLigneCmdFourniseur = idLigneCmdFourniseur;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public CommandeFournisseur getCommandeFournisseur() {
		return commandeFournisseur;
	}

	public void setCommandeFournisseur(CommandeFournisseur commandeFournisseur) {
		this.commandeFournisseur = commandeFournisseur;
	}

}